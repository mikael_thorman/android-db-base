# Purpose

This is a Java library that contains extended base classes for accessing the local database on an Android device.

## So when should I use this

When you have need to Create/Read/Update/Delete data in the local Sqlite database in an android device.

# Library Usage

TBD
 
## Maven dependency

You need to add the following maven dependency.

    <dependency>
		<groupId>eu.miman.android.util</groupId>
		<artifactId>android-db-base</artifactId>
		<version>1.0.1-RELEASE</version>
    </dependency>
