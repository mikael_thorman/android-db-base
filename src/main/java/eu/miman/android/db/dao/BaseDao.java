/*******************************************************************************
 * Copyright 2014 MiMan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package eu.miman.android.db.dao;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.util.Log;
import eu.miman.android.db.DatabaseConnection;
import eu.miman.android.db.exceptions.ResourceNotFoundException;

/**
 * The base class for the DAO's
 * We do not set any PK id, while this is created automatically by SqlLite (ROWID), se this link:
 * http://www.sqlite.org/autoinc.html
 */
public abstract class BaseDao {
	private static final String LOG_TAG = BaseDao.class.getSimpleName();

	public static final String ID = "rowid";
	protected DatabaseConnection dbConnection = null;
	
	public abstract String getTableName();
	protected abstract String[] getAllColumns();
	
	public BaseDao(DatabaseConnection dbConnection) {
		this.dbConnection = dbConnection;
	}
	
	/**
	 * Returns the database id for the current position in the cursor
	 * @param c
	 * @return
	 */
	protected Long getRowId(Cursor c) {
		return c.getLong(c.getColumnIndex(ID));
	}
	
	/**
	 * deletes the picture with the internal db id.
	 * @param id The ROWID
	 * @return
	 */
	public void deleteByRowId(long id) {
		Log.d(LOG_TAG, "deleteByRowId (id=" + id + ")");
		SQLiteDatabase db = dbConnection.getWritableDatabase();
		String whereClause = ID + "=?";
		String[] whereArgs = new String[]{Long.toString(id)};
		
		db.delete(getTableName(), whereClause, whereArgs);
	}
	
	//===========================================
	// help functions	
	/**
	 * Opens a cursor to the object with the given id
	 * @param id
	 * @return
	 */
	protected Cursor findCursorByRowId(long id, SQLiteDatabase db) throws ResourceNotFoundException {
		SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
        qb.setTables(getTableName());
        
        String whereExtId = ID + "=?";
        Cursor c = qb.query(db, null, whereExtId, new String[]{Long.toString(id)}, null, null, null);
        if (!c.moveToFirst()) {
        	// There didn't exist any object with the given key
        	c.close();
        	throw new ResourceNotFoundException();
        }
		return c;
	}

	/**
	 * Opens a cursor to the object with the given id
	 * @param id
	 * @return
	 */
	protected Cursor findCursorForAll(SQLiteDatabase db) throws ResourceNotFoundException {
		SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
        qb.setTables(getTableName());
        
        Cursor c = qb.query(db, getAllColumns(), null, null, null, null, null);
        if (!c.moveToFirst()) {
        	// There didn't exist any object with the given key
        	c.close();
        	throw new ResourceNotFoundException();
        }
		return c;
	}

	/**
	 * Opens a cursor to the object with the given id and the given columns
	 * @param id
	 * @return
	 */
	protected Cursor findCursorByRowIdForColumns(long id, String[] columns, SQLiteDatabase db) throws ResourceNotFoundException {
		SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
        qb.setTables(getTableName());
        
        String whereExtId = ID + "=?";
        Cursor c = qb.query(db, columns, whereExtId, new String[]{Long.toString(id)}, null, null, null);
        if (!c.moveToFirst()) {
        	// There didn't exist any object with the given key
        	c.close();
        	throw new ResourceNotFoundException();
        }
		return c;
	}
}
