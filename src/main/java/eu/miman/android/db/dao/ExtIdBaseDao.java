/**
 * 
 */
package eu.miman.android.db.dao;

import java.util.ArrayList;
import java.util.List;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.util.Log;
import eu.miman.android.db.DatabaseConnection;
import eu.miman.android.db.dao.BaseDao;
import eu.miman.android.db.exceptions.RepositoryStoreException;
import eu.miman.android.db.exceptions.ResourceNotFoundException;
import eu.miman.util.to.objectwithid.ObjectWithId;

/**
 * @author Mikael Thorman
 *
 */
public abstract class ExtIdBaseDao extends BaseDao {

	public static final String EXT_ID = "ext_id";

	protected abstract String getTag();
	
	public ExtIdBaseDao(DatabaseConnection dbConnection) {
		super(dbConnection);
	}

	/**
	 * Loads the location with the external id with all data set including the
	 * pictures.
	 * 
	 * @param extId
	 * @return
	 * @throws ResourceNotFoundException
	 */
	public ObjectWithId findWithExtId(String extId) throws ResourceNotFoundException {
		Log.d(getTag(), "findWithExtId, extId=" + extId);
		ObjectWithId result = null;
		Cursor c = null;
		try {
			c = findCursorByExtId(extId);
			if (c.moveToFirst()) {
				// There are at least one element in the result
				result = createNewEntity();
				loadFromCursor(result, c);
				Log.d(getTag(), "Object found: " + result.toString());
				return result;
			}
			return null;
		} finally {
			if (c != null) {
				c.close();	
			}
		}
	}

	protected abstract ObjectWithId createNewEntity();

	/**
	 * deletes the location with the external id.
	 * 
	 * @param id
	 *            The ext id
	 * @return
	 */
	public void deleteByExtId(String id) {
		Log.d(getTag(), "deleteByExtId");
		try {
			Cursor c = findCursorByExtId(id);
			try {
				// Delete the actual LOCATION
				SQLiteDatabase db = dbConnection.getWritableDatabase();
				String whereClause = EXT_ID + "=?";
				String[] whereArgs = new String[] { id };

				db.delete(getTableName(), whereClause, whereArgs);
			} finally {
				if (c != null) {
					c.close();	
				}
			}
		} catch (ResourceNotFoundException e) {
			// There doesn't exist any member with the given id, delete is not
			// necessary
		}
	}

	/**
	 * Loads all locations with all base data.
	 * 
	 * @return
	 */
	public List<ObjectWithId> findAll() {
		Log.d(getTag(), "findAll");
		List<ObjectWithId> result = new ArrayList<ObjectWithId>();
		SQLiteDatabase db = dbConnection.getReadableDatabase();
		SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
		qb.setTables(getTableName());

		Cursor c = qb.query(db, getAllColumns(), null, null, null, null, null);
		try {
			if (c.moveToFirst()) {
				// There are at least one element in the result
				do {
					ObjectWithId to = createNewEntity();
					loadFromCursor(to, c);
					result.add(to);
				} while (c.moveToNext());
			}
			return result;
		} finally {
			c.close();
		}
	}

	/**
	 * Store the given list of locations to the database
	 * @param list List of locations to store
	 * @throws RepositoryStoreException
	 */
	public void storeAll(List<ObjectWithId> list) throws RepositoryStoreException {
		Log.d(getTag(), "storeAll");
		SQLiteDatabase db = dbConnection.getWritableDatabase();
		for (ObjectWithId posTO : list) {
			Cursor c = null;
			try {
				c = findCursorByExtId(posTO.getId());
				Long rowId = getRowId(c);
				Log.d(getTag(), "updating existing ObjectWithId, ExtId: " + posTO.getId());
				deleteByRowId(rowId);
				storeNew(db, posTO);
			} catch (ResourceNotFoundException e) {
				// ObjectWithId doesn't exist -> this is a new one
				Log.d(getTag(), "storing new ObjectWithId, ExtId: " + posTO.getId());
				storeNew(db, posTO);
			} finally {
				if (c != null) {
					c.close();	
				}
			}
		}
	}

	/**
	 * Store the given location to the database
	 * @param pos The location to store
	 * @throws RepositoryStoreException
	 */
	public void store(ObjectWithId pos) throws RepositoryStoreException {
		Log.d(getTag(), "store: " + pos.toString());
		SQLiteDatabase db = dbConnection.getWritableDatabase();
		Cursor c = null;
		try {
			c = findCursorByExtId(pos.getId());
			Long rowId = getRowId(c);
			Log.d(getTag(), "updating existing object, ExtId: " + pos.getId());
			deleteByRowId(rowId);
			storeNew(db, pos);
		} catch (ResourceNotFoundException e) {
			// object doesn't exist -> this is a new one
			Log.d(getTag(), "storing new object, ExtId: " + pos.getId());
			storeNew(db, pos);
		} finally {
			if (c != null) {
				c.close();	
			}
		}
	}

	/**
	 * Stores a new location to the database.
	 * 
	 * @param db
	 *            The db where to store the new object
	 * @param obj
	 *            The object to store
	 * @throws RepositoryStoreException
	 */
	protected abstract void storeNew(SQLiteDatabase db, ObjectWithId obj)
			throws RepositoryStoreException;

	// ===========================================
	// ----------- help functions
	/**
	 * Opens a cursor to the offer with the given id
	 * 
	 * @param extId
	 * @return
	 */
	private Cursor findCursorByExtId(String extId) throws ResourceNotFoundException {
		if (extId == null || extId.length() == 0) {
			throw new ResourceNotFoundException();
		}
		SQLiteDatabase db = dbConnection.getReadableDatabase();
		SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
		qb.setTables(getTableName());

		String whereExtId = EXT_ID + "=?";
		Cursor c = qb.query(db, getAllColumns(), whereExtId, new String[] { extId }, null, null, null);
		if (!c.moveToFirst()) {
			// There didn't exist any object with the given key
			c.close();
			throw new ResourceNotFoundException();
		}
		return c;
	}

	/**
	 * Loads the object from the db
	 * @param pos
	 * @param c
	 */
	protected abstract void loadFromCursor(ObjectWithId pos, Cursor c);

}
