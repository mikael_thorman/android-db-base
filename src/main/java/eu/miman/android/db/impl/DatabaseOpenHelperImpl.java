/*******************************************************************************
 * Copyright 2014 MiMan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package eu.miman.android.db.impl;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DatabaseOpenHelperImpl extends SQLiteOpenHelper {
	private static final String TAG = "DatabaseOpenHelperImpl";
	/**
	 * These are the tables in this Db
	 */
	private String[] tableNames;
	/**
	 * This is the script to create the entire db
	 */
	private String[] createDbScripts;

	public DatabaseOpenHelperImpl(Context context, String dbName, CursorFactory factory, int version,
			String[] tableNameList, String[] createDbScript) {
		super(context, dbName, factory, version);
		this.tableNames = tableNameList;
		this.createDbScripts = createDbScript;
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		Log.i(TAG, "Trying to create database table if it isn't existed [ " + createDbScripts + " ].");
		try {
			for (String dbScript : createDbScripts) {
				db.execSQL(dbScript);	
			}
		} catch (SQLException se) {
			Log.e(TAG,
					"Cound not create the database table according to the SQL statement [ " + createDbScripts + " ].",
					se);
		}
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		Log.i(TAG, "Upgrading database from version " + oldVersion + " to " + newVersion
				+ ", which will destroy all old data");
		for (String tableName : tableNames) {
			try {
				db.execSQL("DROP TABLE IF EXISTS " + tableName);
			} catch (SQLException se) {
				Log.e(TAG, "Cound not drop the database table [ " + tableName + " ].", se);
			}
		}
		onCreate(db);
	}
}
