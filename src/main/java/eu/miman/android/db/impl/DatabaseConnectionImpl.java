/*******************************************************************************
 * Copyright 2014 MiMan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package eu.miman.android.db.impl;

import eu.miman.android.db.DatabaseConnection;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.util.Log;

public abstract class DatabaseConnectionImpl extends DatabaseOpenHelperImpl implements DatabaseConnection {

    private static final String TAG = "DatabaseConnectionImpl";
    public static final String KEY_ID = "_id";
    String dbName;
    
    public DatabaseConnectionImpl(Context context, String dbName, CursorFactory factory, int version,
    		String[] tableNameList, String[] createDbScript){
        super(context, dbName, factory, version, tableNameList, createDbScript);
        this.dbName = dbName;
        Log.i(TAG, "Creating or opening database [ " + dbName + " ].");
    }

    public DatabaseConnectionImpl(Context context, String dbName, int version,
    		String[] tableNameList, String[] createDbScript){
        super(context, dbName, null, version, tableNameList, createDbScript);
        this.dbName = dbName;
        Log.i(TAG, "Creating or opening database [ " + dbName + " ].");
    }

	@Override
	public String getDatabaseName() {
		return dbName;
	}
}
