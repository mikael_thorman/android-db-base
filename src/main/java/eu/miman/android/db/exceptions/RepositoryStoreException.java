/*******************************************************************************
 * Copyright 2014 MiMan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package eu.miman.android.db.exceptions;

/**
 * This is thrown when we failed to store data to an onboard repository
 * 
 * @author Mikael Thorman
 */
public class RepositoryStoreException extends Exception {

	public RepositoryStoreException() {
		super();
	}

	public RepositoryStoreException(String detailMessage, Throwable throwable) {
		super(detailMessage, throwable);
	}

	public RepositoryStoreException(String detailMessage) {
		super(detailMessage);
	}

	public RepositoryStoreException(Throwable throwable) {
		super(throwable);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = -238727590936412952L;

}
