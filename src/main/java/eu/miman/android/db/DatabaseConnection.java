/*******************************************************************************
 * Copyright 2014 MiMan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package eu.miman.android.db;

import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;

public interface DatabaseConnection {
	/**
	 * Creates and/or open a database for reading.
	 * 
	 * @return
	 * @throws SQLiteException
	 *             if the database cannot be opened
	 */
	SQLiteDatabase getReadableDatabase() throws SQLiteException;

	/**
	 * Create and/or open a database that will be used for reading and writing.
	 * 
	 * The first time this is called, the database will be opened and
	 * onCreate(SQLiteDatabase), onUpgrade(SQLiteDatabase, int, int) and/or
	 * onOpen(SQLiteDatabase) will be called.
	 * 
	 * Once opened successfully, the database is cached, so you can call this
	 * method every time you need to write to the database. (Make sure to call
	 * close() when you no longer need the database.) Errors such as bad
	 * permissions or a full disk may cause this method to fail, but future
	 * attempts may succeed if the problem is fixed.
	 * 
	 * @return
	 * @throws SQLiteException
	 *             if the database cannot be opened
	 */
	SQLiteDatabase getWritableDatabase() throws SQLiteException;
	
	String getDatabaseName();
}
